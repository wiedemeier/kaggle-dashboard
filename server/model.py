import pandas as pd

from sklearn.compose import ColumnTransformer
from sklearn.ensemble import RandomForestClassifier
from sklearn.impute import SimpleImputer
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import OneHotEncoder

data = pd.read_csv("./data/train_transaction.csv")[[
    "isFraud",
    "TransactionDT",
    "TransactionAmt",
    "ProductCD",
    "card1",
    "card2",
    "card3",
    "card4",
    "card5",
    "card6",
    "addr1",
    "addr2",
    "dist1",
    "dist2",
    "P_emaildomain",
    "R_emaildomain"
]]

# Basic model training
y = data.isFraud
X = data.drop(columns=["isFraud"])

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25)

# One hot encode categorical (enumerable) columns
categorical_columns = ["ProductCD", "card4", "card6", "P_emaildomain", "R_emaildomain"]
numeric_columns = [c for c in X_train.columns if c not in categorical_columns]

categorical_pipeline = Pipeline([
    ("imputer", SimpleImputer(strategy="constant", fill_value="missing")),
    ("encoder", OneHotEncoder(handle_unknown="ignore", sparse=False))
])

numeric_pipeline = Pipeline([
    ("imputer", SimpleImputer(strategy="constant", fill_value=0))
])

# Combine pipelines as preprocessor
preprocessor = ColumnTransformer([
    ("categorical_pipeline", categorical_pipeline, categorical_columns),
    ("numeric_pipeline", numeric_pipeline, numeric_columns)
])

random_forest_pipeline = Pipeline([
    ("preprocessor", preprocessor),
    ("model", RandomForestClassifier())
])

random_forest_pipeline.fit(X_train, y_train)

y_preds = random_forest_pipeline.predict(X_test)
print("y_preds roc_auc: ", roc_auc_score(y_test, y_preds))

y_preds_proba = random_forest_pipeline.predict_proba(X_test)[:, 1]
print("y_preds_proba roc_auc: ", roc_auc_score(y_test, y_preds_proba))
