import math

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy import stats

data = pd.read_csv("./data/train_transaction.csv")[[
    "TransactionID",
    "isFraud",
    "TransactionDT",
    "TransactionAmt",
    "ProductCD",
    "card1",
    "card2",
    "card3",
    "card4",
    "card5",
    "card6",
    "addr1",
    "addr2",
    "dist1",
    "dist2",
    "P_emaildomain",
    "R_emaildomain"
]]

# from sqlalchemy import create_engine

# TABLE_NAME = "jtw_train_transaction"
# DATABASE_URL = "postgresql://asr:asr-kaggle-training@training:5432/asr"
# engine = create_engine(DATABASE_URL)
# data.to_sql(TABLE_NAME, engine, index=False)

legit_data = data[data["isFraud"] == 0]

# Remove outliers
legit_data = legit_data[np.abs(stats.zscore(data["TransactionAmt"]) < 30)]

fraud_data = data[data["isFraud"] == 1]

#################################################
# Summary Stats
#################################################

summary_stats = {}


def aggregate_by_day(data):
    # Bin TransactionDT by days (86400 seconds)
    upper_bin = math.floor(data["TransactionDT"].max() / 86400)
    lower_bin = math.ceil(data["TransactionDT"].min() / 86400)

    time_bins = list(map(lambda n: n * 86400, range(lower_bin, upper_bin)))

    day_binned_data = data.groupby(pd.cut(data["TransactionDT"], time_bins))

    return day_binned_data


time_binned_amounts = aggregate_by_day(data)["TransactionAmt"].agg(["count", "sum", "mean"])

summary_stats["transactions_per_day"] = round(time_binned_amounts["count"].sum() / time_binned_amounts.shape[0])
summary_stats["transaction_amount_per_day"] = round(time_binned_amounts["sum"].mean(), 2)

fraud_time_binned_amounts = aggregate_by_day(fraud_data)["TransactionAmt"].agg(["count", "sum", "mean"])

summary_stats["fraud_transactions_per_day"] = round(
    fraud_time_binned_amounts["count"].sum() / fraud_time_binned_amounts.shape[0])
summary_stats["fraud_transaction_amount_per_day"] = round(fraud_time_binned_amounts["sum"].mean(), 2)

print(summary_stats)


#################################################
# Aggregations
#################################################

def plot_amt(data):
    return plt.hist(data[np.abs(stats.zscore(data["TransactionAmt"]) < 30)]["TransactionAmt"],
                    # bins=[0, 1, 2.2, 4.7, 10, 22, 47, 100, 220, 470, 1000, 2200, 4700, 10000],
                    bins=1.5 ** (np.arange(0, 20)),
                    # log=True,
                    density=True
                    )


def plot_amt_alt(data):
    return plt.hist(data[np.abs(stats.zscore(data["TransactionAmt"]) < 30)]["TransactionAmt"],
                    # bins=[0, 1, 2.2, 4.7, 10, 22, 47, 100, 220, 470, 1000, 2200, 4700, 10000],
                    log=True,
                    density=True
                    )


plt.close()

ax = plt.subplot(2, 1, 1)
plt.xscale("log")
plt.ylim([0, 0.011])
ax.text(.68, .9, "legitimate, n=" + str(legit_data.shape[0]),
        transform=ax.transAxes)
plot_amt(legit_data)

ax = plt.subplot(2, 1, 2)
plt.xscale("log")
plt.ylim([0, 0.011])
ax.text(.68, .9, "fraudulent, n=" + str(fraud_data.shape[0]),
        transform=ax.transAxes)
plt.xlabel("Transaction Amount")

plot_amt(fraud_data)

plt.show()


# plt.close()
#
# ax = plt.subplot(2, 1, 1)
# ax.text(.68, .9, "legitimate, n=" + str(legit_data.shape[0]),
#         transform=ax.transAxes)
# plot_amt_alt(legit_data)
#
# ax = plt.subplot(2, 1, 2)
# ax.text(.68, .9, "fraudulent, n=" + str(fraud_data.shape[0]),
#         transform=ax.transAxes)
# plt.xlabel("Transaction Amount")
#
# plot_amt_alt(fraud_data)
#
# plt.show()


# . . . . . . . . . . . . . . . . #


def plot_bar(col, width=.45):
    plt.close()

    y = legit_data[col].value_counts().map(lambda n: n / legit_data.shape[0])
    x = y.axes[0]

    y2 = fraud_data[col].value_counts().map(lambda n: n / fraud_data.shape[0])
    y2 = x.map(lambda card: y2[card])

    x_pos = [i for i, _ in enumerate(x)]
    x2_pos = [i + width for i, _ in enumerate(x)]

    plt.bar(x_pos, y, width=width, label="legit, n= " + str(legit_data.shape[0]))
    plt.bar(x2_pos, y2, width=width, label="fraud, n= " + str(fraud_data.shape[0]))
    plt.xticks([i + width / 2 for i, _ in enumerate(x)], x)
    plt.legend()

    plt.show()


plot_bar("card4")
plot_bar("ProductCD")


# . . . . . . . . . . . . . . . . #


def plot_hist(col, log=True, bins=20):
    plt.close()

    ax = plt.subplot(2, 1, 1)
    ax.text(.68, .9, "legitimate, n=" + str(legit_data.shape[0]),
            transform=ax.transAxes)
    ar = plt.hist(legit_data[col], log=log, density=True, bins=bins)
    print("legit", ar[0])
    max1 = np.max(ar[0])

    ax2 = plt.subplot(2, 1, 2)
    ax2.text(.68, .9, "fraudulent, n=" + str(fraud_data.shape[0]),
             transform=ax2.transAxes)
    ar2 = plt.hist(fraud_data[col], log=log, density=True, bins=bins)
    print("fraud", ar2[0])
    max2 = np.max(ar2[0])

    ylim = np.max([max1, max2])
    ax.set_ylim([0, ylim * 0.1 + ylim])
    ax2.set_ylim([0, ylim * 0.1 + ylim])

    plt.show()


plot_hist("TransactionAmt")

plot_hist("dist1")
plot_hist("dist2")

plot_hist("card1")

plot_hist("card2")
plot_hist("card3")
plot_hist("card5")
