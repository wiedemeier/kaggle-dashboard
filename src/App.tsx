import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import SummaryStats from "./components/SummaryStats";
import Aggregations from "./components/Aggregations";
import ModelStats from "./components/ModelStats";

import "./App.css";

const App: React.FC = () => {
  return (
    <div id="app">
      <audio id="keystroke" src="keystroke.wav" />
      <Router>
        <Routes>
          <Route path="*" element={<SummaryStats />} />
          <Route path="/aggregations" element={<Aggregations />} />
          <Route path="/stats" element={<ModelStats />} />
        </Routes>
      </Router>
    </div>
  );
};

export default App;
