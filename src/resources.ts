export const unveilElements = (parentElement: Element | null): void => {
  const elementsToUnveil: Element[] = [];

  // Add elements to array by preorder tree traversal
  (function searchForChildElements(element: Element | null) {
    if (!element) return;

    // Push parent if it needs unveiling
    if (element.hasAttribute("data-unveil")) elementsToUnveil.push(element);

    // Iterate over children
    Array.from(element.children).forEach((childElement) => {
      searchForChildElements(childElement);
    });
  })(parentElement);

  let unveilIndex = 0;

  (function unveilElement(element: Element | null) {
    if (!element) return;
    if (unveilIndex >= elementsToUnveil.length) return;

    const unveilClassName = element.getAttribute("data-unveil");
    if (!unveilClassName) return;

    const options = JSON.parse(
      element.getAttribute("data-unveil-options") || "",
    );

    if (process.env.NODE_ENV === "development") {
      options.duration *= 0.6;
    } else {
      options.duration *= 0.6;
    }

    // Unveil according to unveilClassName
    switch (unveilClassName) {
      case "typeout": {
        const txt = (element as HTMLTextAreaElement).innerText;

        let activeTxt = String.fromCharCode(160).repeat(txt.length);
        (element as HTMLTextAreaElement).innerText = activeTxt;

        element.classList.remove(unveilClassName);

        let txtIndex = 0;

        // === Keystroke time = avgStrokeTime +/- strokeTimeVariation %
        const avgStrokeTime = (0.9 * options.duration) / txt.length;
        const strokeTimeVariation = 0.4;
        const strokeTimes = Array(txt.length)
          .fill("")
          .map(
            () =>
              avgStrokeTime -
              strokeTimeVariation * avgStrokeTime +
              2 * strokeTimeVariation * Math.random() * avgStrokeTime,
          );

        (function typeCharacter(i: number) {
          if (i === txt.length) return;

          activeTxt =
            activeTxt.substr(0, txtIndex) +
            txt.substr(txtIndex, 1) +
            activeTxt.substr(txtIndex + 1);
          (element as HTMLTextAreaElement).innerText = activeTxt;

          const keystrokeAudioNode = document.getElementById(
            "keystroke",
          ) as HTMLAudioElement;
          if (keystrokeAudioNode) {
            keystrokeAudioNode.volume = 0.05 + 0.05 * Math.random();
            keystrokeAudioNode.play().catch((err) => {});
          }

          txtIndex += 1;
          setTimeout(() => {
            typeCharacter(txtIndex);
          }, strokeTimes[txtIndex]);
        })(txtIndex);

        break;
      }

      default:
        element.classList.remove(unveilClassName);
        break;
    }

    unveilIndex += 1;
    setTimeout(() => {
      unveilElement(elementsToUnveil[unveilIndex]);
    }, options.duration || 0);
  })(elementsToUnveil[unveilIndex]);
};
