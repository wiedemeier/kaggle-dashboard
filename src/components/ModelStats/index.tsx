import React, { useEffect } from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Chart } from "react-chartjs-2";
import { unveilElements } from "../../resources";
import "./ModelStats.css";
import { useNavigate } from "react-router-dom";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
);

const ModelStats: React.FC = () => {
  useEffect(() => {
    unveilElements(document.getElementById("model-stats"));
  }, []);
  const navigate = useNavigate();

  return (
    <div
      className="module-container"
      id="model-stats"
      // style={{ maxHeight: "700px" }}
    >
      <div>
        <p className="data-bar-title">
          <span
            className="redtxt largetxt typeout"
            data-unveil="typeout"
            data-unveil-options={JSON.stringify({ duration: 1000 })}
          >
            113
          </span>
          <span
            className="typeout"
            data-unveil="typeout"
            data-unveil-options={JSON.stringify({ duration: 1300 })}
          >
            {" "}
            daily fraudulent
          </span>
          <span
            className="typeout"
            data-unveil="typeout"
            data-unveil-options={JSON.stringify({ duration: 1500 })}
          >
            {" "}
            transactions totaling
          </span>
          <span
            className="redtxt largetxt typeout"
            data-unveil="typeout"
            data-unveil-options={JSON.stringify({ duration: 1200 })}
          >
            {" "}
            $16,867.37
          </span>
        </p>
      </div>
      <div>
        <div
          className="data-bar shrink"
          data-unveil="shrink"
          data-unveil-options={JSON.stringify({ duration: 1000 })}
        >
          <div
            className="data-bar-fill shrink green"
            style={{ transform: `scaleX(${0.5})` }}
            data-unveil="shrink"
            data-unveil-options={JSON.stringify({ duration: 1000 })}
          />
        </div>
      </div>
      <div>
        <p className="data-bar-title" style={{ marginTop: "15px" }}>
          <span
            className="greentxt largetxt typeout"
            data-unveil="typeout"
            data-unveil-options={JSON.stringify({ duration: 1000 })}
          >
            66
          </span>
          <span
            className="typeout"
            data-unveil="typeout"
            data-unveil-options={JSON.stringify({ duration: 1300 })}
          >
            {" "}
            transactions would
          </span>
          <span
            className="typeout"
            data-unveil="typeout"
            data-unveil-options={JSON.stringify({ duration: 1500 })}
          >
            {" "}
            be prevented totalling
          </span>
          <span
            className="greentxt largetxt typeout"
            data-unveil="typeout"
            data-unveil-options={JSON.stringify({ duration: 1200 })}
          >
            {" "}
            $9,851.74
          </span>
        </p>
      </div>
      <div className="divider">
        <div
          className="hide"
          data-unveil="hide"
          data-unveil-options={JSON.stringify({ duration: 1500 })}
        >
          <table id="confusion-table">
            <tbody>
              <tr className="smalltxt">
                <td> </td>
                <td className="greytxt">Target +</td>
                <td className="greytxt">Target -</td>
              </tr>
              <tr>
                <td className="greytxt smalltxt">Model +</td>
                <td className="largetxt greentxt">37.7%</td>
                <td className="largetxt redtxt">8.2%</td>
              </tr>
              <tr>
                <td className="greytxt smalltxt">Model -</td>
                <td className="largetxt redtxt">4.9%</td>
                <td className="largetxt greentxt">49.2%</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div
          style={{ height: "260px", width: "330px", position: "relative" }}
          className="hide"
          data-unveil="hide"
          data-unveil-options={JSON.stringify({ duration: 1500 })}
        >
          <Chart
            type="scatter"
            options={{
              maintainAspectRatio: false,
              scales: {
                x: { display: false },
                y: { display: false },
              },
              plugins: { legend: { display: false } },
            }}
            data={{
              datasets: [
                {
                  data: [
                    { x: 0, y: 0 },
                    { x: 0.3, y: 0.5 },
                    { x: 0.6, y: 0.75 },
                    { x: 1, y: 1 },
                  ],
                  showLine: true,
                  fill: false,
                  borderColor: "#81f499",
                },
                {
                  data: [
                    { x: 0, y: 0 },
                    { x: 1, y: 1 },
                  ],
                  showLine: true,
                  fill: false,
                  borderColor: "#767071",
                },
              ],
            }}
          />
          <p id="aoc-label" className="largetxt greentxt">
            0.63 <span className="smalltxt greytxt">AOC</span>
          </p>
        </div>
      </div>
      <button
        type="button"
        className="next-page-button"
        onClick={() => {
          document.getElementById("model-stats")?.classList.add("hide");
          setTimeout(() => {
            navigate("/");
          }, 900);
        }}
      >
        <p
          className="typeout smalltxt"
          data-unveil="typeout"
          data-unveil-options={JSON.stringify({ duration: 1400 })}
        >
          test our model
        </p>
        <p
          className="typeout smalltxt"
          data-unveil="typeout"
          data-unveil-options={JSON.stringify({ duration: 350 })}
        >
          vvv
        </p>
      </button>
    </div>
  );
};

export default ModelStats;
