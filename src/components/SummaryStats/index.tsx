import React, { useEffect } from "react";
import { unveilElements } from "../../resources";
import { useNavigate } from "react-router-dom";

const SummaryStats: React.FC = () => {
  useEffect(() => {
    unveilElements(document.getElementById("summary-stats"));
  }, []);
  const navigate = useNavigate();

  return (
    <div className="module-container" id="summary-stats">
      <div>
        <p className="data-bar-title">
          <span
            className="bluetxt largetxt typeout"
            data-unveil="typeout"
            data-unveil-options={JSON.stringify({ duration: 1200 })}
          >
            3,250
          </span>
          <span
            className="typeout"
            data-unveil="typeout"
            data-unveil-options={JSON.stringify({ duration: 3000 })}
          >
            {" "}
            daily credit card transactions
          </span>
        </p>
        <div
          className="data-bar shrink"
          data-unveil="shrink"
          data-unveil-options={JSON.stringify({ duration: 1000 })}
          style={{ borderColor: "var(--blue)" }}
        >
          <p>
            <span
              className="typeout"
              data-unveil="typeout"
              data-unveil-options={JSON.stringify({ duration: 1000 })}
            >
              totaling{" "}
            </span>
            <span
              className="bluetxt largetxt typeout"
              data-unveil="typeout"
              data-unveil-options={JSON.stringify({ duration: 3000 })}
            >
              $438,022.79
            </span>
          </p>
          <div
            className="data-bar-fill shrink"
            style={{ transform: `scaleX(${16867.37 / 438022.79})` }}
            data-unveil="shrink"
            data-unveil-options={JSON.stringify({ duration: 1000 })}
          />
        </div>
      </div>
      <div>
        <p className="data-bar-title">
          <span
            className="redtxt largetxt typeout"
            data-unveil="typeout"
            data-unveil-options={JSON.stringify({ duration: 1000 })}
          >
            113
          </span>
          <span
            className="typeout"
            data-unveil="typeout"
            data-unveil-options={JSON.stringify({ duration: 1500 })}
          >
            {" "}
            transactions are
          </span>
          <span
            className="redtxt typeout"
            data-unveil="typeout"
            data-unveil-options={JSON.stringify({ duration: 1000 })}
          >
            {" "}
            fraudulent
          </span>
        </p>
        <div
          className="data-bar shrink"
          data-unveil="shrink"
          data-unveil-options={JSON.stringify({ duration: 1000 })}
          style={{ borderColor: "var(--red)" }}
        >
          <p>
            <span
              className="typeout"
              data-unveil="typeout"
              data-unveil-options={JSON.stringify({ duration: 800 })}
            >
              totaling{" "}
            </span>
            <span
              className="redtxt largetxt typeout"
              data-unveil="typeout"
              data-unveil-options={JSON.stringify({ duration: 2000 })}
            >
              $16,867.37
            </span>
          </p>
        </div>
      </div>
      <button
        type="button"
        className="next-page-button"
        onClick={() => {
          document.getElementById("summary-stats")?.classList.add("hide");
          setTimeout(() => {
            navigate("/aggregations");
          }, 900);
        }}
      >
        <p
          className="typeout smalltxt"
          data-unveil="typeout"
          data-unveil-options={JSON.stringify({ duration: 2000 })}
        >
          compare transaction details
        </p>
        <p
          className="typeout smalltxt"
          data-unveil="typeout"
          data-unveil-options={JSON.stringify({ duration: 350 })}
        >
          vvv
        </p>
      </button>
    </div>
  );
};

export default SummaryStats;
