import React, { useEffect, useState } from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Bar } from "react-chartjs-2";
import { unveilElements } from "../../resources";
import "./Aggregations.css";
import { useNavigate } from "react-router-dom";

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
);

const aggregationTabs: { [title: string]: string } = {
  type: "card6",
  amount: "TransactionAmtBin",
  card: "card4",
  email: "P_emaildomain",
  distance1: "Dist1Bin",
  distance2: "Dist2Bin",
};

const Aggregations: React.FC = () => {
  const [data, setData] = useState<null | { [col: string]: number[] }>(null);
  const [aggregationTab, setAggregationTab] = useState<string>("amount");
  const colName = aggregationTabs[aggregationTab];

  const navigate = useNavigate();

  useEffect(() => {
    unveilElements(document.getElementById("aggregations"));
  }, []);

  useEffect(() => {
    fetch("/api/aggregate-data")
      .then((res) => res.json())
      .then((json) => {
        console.log("data", json);
        setData(json);
      });
  }, []);

  return (
    <div className="module-container" id="aggregations">
      <div id="aggregation-tab-wrapper">
        {Object.keys(aggregationTabs).map((title) => (
          <button
            type="button"
            className={aggregationTab === title ? "active" : ""}
            key={title}
            onClick={() => {
              setAggregationTab(title);
            }}
          >
            <p
              className="smalltxt typeout"
              data-unveil="typeout"
              data-unveil-options={JSON.stringify({
                duration: title.length * 120,
              })}
            >
              {title}
            </p>
          </button>
        ))}
      </div>
      <div
        id="data-comparison"
        className="hide"
        data-unveil="hide"
        data-unveil-options={JSON.stringify({ duration: 1100 })}
      >
        {data ? (
          <Bar
            data={{
              labels: Object.keys(data[colName]),
              datasets: [
                {
                  label: "Fraud Rate",
                  data: Object.values(data[colName]).map((counts) => {
                    const [fraudCount, totalCount] =
                      counts as unknown as number[];
                    return fraudCount / totalCount;
                  }),
                  borderWidth: 1,
                  borderRadius: 5,
                  backgroundColor: "transparent",
                  borderColor: "#db3a34",
                },
              ],
            }}
            options={{
              plugins: {
                legend: {
                  display: false,
                },
              },
              responsive: true,
              scales: {
                x: {
                  ticks: {
                    color: "#f7ebec",
                    font: { size: 15, family: "monospace" },
                  },
                  grid: {
                    display: false,
                  },
                },
                y: {
                  title: {
                    display: true,
                    text: "Fraud Rate",
                    color: "#a6a0a1",
                    font: { size: 16, family: "monospace" },
                  },
                  ticks: {
                    color: "#a6a0a1",
                    font: { size: 15, family: "monospace" },
                  },
                  grid: {
                    display: false,
                  },
                },
              },
            }}
          />
        ) : (
          "Loading..."
        )}
      </div>
      <button
        type="button"
        className="next-page-button"
        onClick={() => {
          document.getElementById("aggregations")?.classList.add("hide");
          setTimeout(() => {
            navigate("/stats");
          }, 900);
        }}
      >
        <p
          className="typeout smalltxt"
          data-unveil="typeout"
          data-unveil-options={JSON.stringify({ duration: 1300 })}
        >
          model statistics
        </p>
        <p
          className="typeout smalltxt"
          data-unveil="typeout"
          data-unveil-options={JSON.stringify({ duration: 350 })}
        >
          vvv
        </p>
      </button>
    </div>
  );
};

export default Aggregations;
